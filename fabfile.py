import os
from fabric import Connection, task


@task
def deploy(ctx):
   with Connection(
       os.environ["MOD_31_HOST"],
       user=os.environ["MOD_31_USER"],
       connect_kwargs={"key_filename": os.environ["MOD_31_PRIVATE_KEY"]}
   ) as c:
       with c.cd("/home/user/workspace/module_31/service"):
           c.run("docker-compose down")
           c.run("git pull origin main --recurse-submodules --rebase")
           c.run("docker-compose up --build -d")
